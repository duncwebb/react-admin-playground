// in src/App.js
import React from 'react';
import { Admin, Resource, ListGuesser } from 'react-admin';
import { UserList } from './users'; 
import { PostList, PostEdit, PostCreate } from './posts';
import Dashboard from './Dashboard';
import authProvider from './authProvider';
import jsonServerProvider from 'ra-data-json-server';
import PostIcon from '@material-ui/icons/Book';
import UserIcon from '@material-ui/icons/Group';
import MyLayout from './MyLayout';


const dataProvider = jsonServerProvider('http://jsonplaceholder.typicode.com');
const App = () => (
<Admin appLayout={MyLayout} dashboard={Dashboard} authProvider={authProvider} dataProvider={dataProvider}>
      <Resource name="users" list={UserList} icon={UserIcon} />
      <Resource name="posts" list={PostList} icon={PostIcon} edit={PostEdit} create={PostCreate}/>
      <Resource name="comments" list={ListGuesser} />
  </Admin>
);

export const PostTitle = ({ record }) => {
  return <span>Qx Dashboard</span>;
};

export default App;
